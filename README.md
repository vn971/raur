[![pipeline status](https://gitlab.com/davidbittner/paurc/badges/master/pipeline.svg)](https://gitlab.com/davidbittner/paurc/commits/master)

# raur

raur is a library for interacting with the
[aurweb RPC Interface](https://aur.archlinux.org/rpc).

See also the [Arch wiki page](https://aur.archlinux.org/rpc.php) for more information.

## Example

```rust
// Use `search` to search using keywords (multiple strategies available)
let pkgs = raur::search("pacman")?;
assert!(pkgs.len() > 10);

for pkg in pkgs {
    println!("{:<30}{}", pkg.name, pkg.version);
}

// Use `info` to get info about a specific packages.
let pkgs = raur::info(&["spotify", "discord"])?;
assert_eq!(pkgs.len(), 2);

for pkg in pkgs {
    println!("{:<30}{}", pkg.name, pkg.version);
}
```

