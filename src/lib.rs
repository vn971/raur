//! # raur
//!
//! raur is a library for interacting with the
//! [aurweb RPC Interface](https://aur.archlinux.org/rpc).
//!
//! See also the [Arch wiki page](https://aur.archlinux.org/rpc.php) for more information.
//!
//! # Example
//!
//! ```
//! # fn main() -> Result<(), raur::Error> {
//! // Use `search` to search using keywords (multiple strategies available)
//! let pkgs = raur::search("pacman")?;
//! assert!(pkgs.len() > 10);
//!
//! for pkg in pkgs {
//!     println!("{:<30}{}", pkg.name, pkg.version);
//! }
//!
//! // Use `info` to get info about a list of packages. Not-found packages are silently ignored.
//! let pkgs = raur::info(&["spotify", "discord"])?;
//! assert_eq!(pkgs.len(), 2);
//!
//! for pkg in pkgs {
//!     println!("{:<30}{}", pkg.name, pkg.version);
//! }
//! # Ok(())
//! # }
//! ```
#[warn(missing_docs)]
mod error;
#[warn(missing_docs)]
mod funcs;
#[warn(missing_docs)]
mod handle;
#[warn(missing_docs)]
mod raur;

pub use crate::error::*;
pub use crate::funcs::*;
pub use crate::handle::*;
pub use crate::raur::*;
