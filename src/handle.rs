use crate::error::Error;
use crate::raur::*;
use reqwest::{Client, Url, UrlError};
use serde_derive::Deserialize;

/// The default URL used for the AUR.
pub static AUR_URL: &'static str = "https://aur.archlinux.org/rpc/";

#[derive(Deserialize)]
struct Response {
    #[serde(rename = "type")]
    response_type: String,
    error: Option<String>,
    results: Vec<Package>,
}

/// A handle for making AUR requests.
#[derive(Clone, Debug)]
pub struct Handle {
    client: Client,
    url: Url,
}

impl Default for Handle {
    fn default() -> Self {
        Handle {
            url: AUR_URL.parse().unwrap(),
            client: reqwest::Client::new(),
        }
    }
}

impl Handle {
    /// Used to manually specify the client that should be used.
    pub fn with_client(mut self, cli: Client) -> Self {
        self.client = cli;
        self
    }

    /// Used to manually specify a URL to be used. If not specified,
    /// aur.archlinux.org/rpc is used.
    pub fn with_url(mut self, url: &str) -> Result<Self, UrlError> {
        let url: Url = Url::parse(url)?;
        self.url = url;
        Ok(self)
    }

    /// Used to retrieve the internal URL. This just points to AUR_URL if you did not explicitly
    /// set it.
    pub fn url(&self) -> Url {
        self.url.clone()
    }

    /// Used to retrieve the client stored inside the struct. This consumes the struct as
    /// duplication could be expensive.
    pub fn client(self) -> Client {
        self.client
    }

    /// A helper function for making a request with given parameters.
    fn request(&self, params: &[(&str, &str)]) -> Result<Vec<Package>, Error> {
        let new_url = Url::parse_with_params(self.url.as_str(), params)?;

        let response: Response = self.client.get(new_url).send()?.json()?;

        if response.response_type == "error" {
            Err(Error::Aur(
                response
                    .error
                    .unwrap_or_else(|| "No error message provided".to_string()),
            ))
        } else {
            Ok(response.results)
        }
    }

    /// Performs an AUR info request.
    ///
    /// This function sends an info request to the AUR RPC. This kind of request takes a list
    /// of package names and returns a list of AUR [`Package`](struct.Package.html)s who's name exactly matches
    /// the input.
    ///
    /// **Note:** If a package being queried does not exist then it will be silently ignored
    /// and not appear in return value.
    ///
    /// **Note:** The return value has no guaranteed order.
    pub fn info<S: AsRef<str>>(&self, pkg_names: &[S]) -> Result<Vec<Package>, Error> {
        let mut params = pkg_names
            .iter()
            .map(|name| ("arg[]", name.as_ref()))
            .collect::<Vec<_>>();
        params.extend(&[("v", "5"), ("type", "info")]);

        self.request(&params)
    }

    /// Performs an AUR search request.
    ///
    /// This function sends a search request to the AUR RPC. This kind of request takes a
    /// single query and returns a list of matching packages.
    ///
    /// **Note:** Unlike info, search results will never inclide:
    ///
    /// - Dependency types
    /// - Licence
    /// - Groups
    ///
    /// See [`SearchBy`](enum.SearchBy.html) for how packages are matched.
    pub fn search_by<S: AsRef<str>>(
        &self,
        query: S,
        strategy: SearchBy,
    ) -> Result<Vec<Package>, Error> {
        self.request(&[
            ("v", "5"),
            ("type", "search"),
            ("by", &strategy.to_string()),
            ("arg", query.as_ref()),
        ])
    }

    /// Performs an AUR search request by NameDesc.
    ///
    /// This is the same as [`fn.search_by`](fn.search_by.html) except it always searches by
    /// NameDesc (the default for the AUR).
    pub fn search<S: AsRef<str>>(&self, query: S) -> Result<Vec<Package>, Error> {
        self.search_by(query, SearchBy::NameDesc)
    }

    /// Returns a list of all orphan packages in the AUR.
    pub fn orphans(&self) -> Result<Vec<Package>, Error> {
        self.search_by("", SearchBy::Maintainer)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_search() {
        let handle = Handle::default();

        let query = handle.search("zzzzzzz").unwrap();
        assert_eq!(0, query.len());

        let query = handle.search("spotify").unwrap();
        assert!(query.len() > 0);
    }

    #[test]
    fn test_info() {
        let handle = Handle::default();

        let query = handle.info(&["pacman-git"]).unwrap();
        assert_eq!(query[0].name, "pacman-git");

        // I maintain these two packages, so I can verify they exist.
        let query = handle.info(&["screens", "screens-git"]);
        assert!(query.is_ok());

        let query = query.unwrap();
        assert_eq!(2, query.len());
    }
}
