use crate::error::Error;

use crate::handle::*;
use crate::raur::*;

/// Performs an AUR search request.
///
/// This function sends a search request to the AUR RPC. This kind of request takes a
/// single query and returns a list of matching packages.
///
/// **Note:** Unlike info, search results will never inclide:
///
/// - Dependency types
/// - Licence
/// - Groups
///
/// See [`SearchBy`](enum.SearchBy.html) for how packages are matched.
pub fn search_by<S: AsRef<str>>(query: S, strategy: SearchBy) -> Result<Vec<Package>, Error> {
    Handle::default().search_by(query, strategy)
}

/// Performs an AUR info request.
///
/// This function sends an info request to the AUR RPC. This kind of request takes a list
/// of package names and returns a list of AUR [`Package`](struct.Package.html)s who's name exactly matches
/// the input.
///
/// **Note:** If a package being queried does not exist then it will be silently ignored
/// and not appear in return value.
///
/// **Note:** The return value has no guaranteed order.
pub fn info<S: AsRef<str>>(pkg_names: &[S]) -> Result<Vec<Package>, Error> {
    Handle::default().info(pkg_names)
}

/// Performs an AUR search request by NameDesc.
///
/// This is the same as [`fn.search_by`](fn.search_by.html) except it always searches by
/// NameDesc (the default for the AUR).
pub fn search<S: AsRef<str>>(query: S) -> Result<Vec<Package>, Error> {
    Handle::default().search(query)
}

/// Returns a list of all orphan packages in the AUR.
pub fn orphans() -> Result<Vec<Package>, Error> {
    Handle::default().orphans()
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_search() {
        let query = search("zzzzzzzzzzz").unwrap();
        assert_eq!(0, query.len());

        let query = search("spotify").unwrap();
        assert!(query.len() > 0);
    }

    #[test]
    fn test_info() {
        let query = info(&["pacman-git"]).unwrap();
        assert_eq!(query[0].name, "pacman-git");

        // I maintain these two packages, so I can verify they exist.
        let query = info(&["screens", "screens-git"]);
        assert!(query.is_ok());

        let query = query.unwrap();
        assert_eq!(2, query.len());
    }

}
